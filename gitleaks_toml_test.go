package main_test

import (
	"fmt"
	"os"
	"regexp"
	"testing"

	"github.com/lucasjones/reggen"
	"github.com/pelletier/go-toml"
	"github.com/stretchr/testify/require"
)

type TOMLRule struct {
	ID    string
	Regex string
}

type GitleaksTOML struct {
	Title string
	Rules []TOMLRule
}

var ExcludedRules = map[string]struct{}{
	"gitlab_personal_access_token":                  {},
	"gitlab_pipeline_trigger_token":                 {},
	"gitlab_runner_registration_token":              {},
	"gitlab_runner_auth_token":                      {},
	"gitlab_feed_token":                             {},
	"AWS":                                           {},
	"PKCS8 private key":                             {},
	"RSA private key":                               {},
	"SSH private key":                               {},
	"PGP private key":                               {},
	"systemd-machine-id":                            {},
	"Github Personal Access Token":                  {},
	"Github OAuth Access Token":                     {},
	"SSH (DSA) private key":                         {},
	"SSH (EC) private key":                          {},
	"Github App Token":                              {},
	"Github Refresh Token":                          {},
	"Shopify shared secret":                         {},
	"Shopify access token":                          {},
	"Shopify custom app access token":               {},
	"Shopify private app access token":              {},
	"Slack token":                                   {},
	"Stripe":                                        {},
	"PyPI upload token":                             {},
	"Google (GCP) Service-account":                  {},
	"GCP API key":                                   {},
	"GCP OAuth client secret":                       {},
	"Password in URL":                               {},
	"Heroku API Key":                                {},
	"Slack Webhook":                                 {},
	"Twilio API Key":                                {},
	"Age secret key":                                {},
	"Facebook token":                                {},
	"Twitter token":                                 {},
	"Adobe Client ID (Oauth Web)":                   {},
	"Adobe Client Secret":                           {},
	"Alibaba AccessKey ID":                          {},
	"Alibaba Secret Key":                            {},
	"Asana Client ID":                               {},
	"Asana Client Secret":                           {},
	"Atlassian API token":                           {},
	"Bitbucket client ID":                           {},
	"Bitbucket client secret":                       {},
	"Beamer API token":                              {},
	"Clojars API token":                             {},
	"Contentful delivery API token":                 {},
	"Contentful preview API token":                  {},
	"Databricks API token":                          {},
	"digitalocean-access-token":                     {},
	"digitalocean-pat":                              {},
	"digitalocean-refresh-token":                    {},
	"Discord API key":                               {},
	"Discord client ID":                             {},
	"Discord client secret":                         {},
	"Doppler API token":                             {},
	"Dropbox API secret/key":                        {},
	"Dropbox short lived API token":                 {},
	"Dropbox long lived API token":                  {},
	"Duffel API token":                              {},
	"Dynatrace API token":                           {},
	"EasyPost API token":                            {},
	"EasyPost test API token":                       {},
	"Fastly API token":                              {},
	"Finicity client secret":                        {},
	"Finicity API token":                            {},
	"Flutterwave public key":                        {},
	"Flutterwave secret key":                        {},
	"Flutterwave encrypted key":                     {},
	"Frame.io API token":                            {},
	"GoCardless API token":                          {},
	"Grafana API token":                             {},
	"Hashicorp Terraform user/org API token":        {},
	"Hashicorp Vault batch token":                   {},
	"Hubspot API token":                             {},
	"Intercom API token":                            {},
	"Intercom client secret/ID":                     {},
	"Ionic API token":                               {},
	"Linear API token":                              {},
	"Linear client secret/ID":                       {},
	"Lob API Key":                                   {},
	"Lob Publishable API Key":                       {},
	"Mailchimp API key":                             {},
	"Mailgun private API token":                     {},
	"Mailgun public validation key":                 {},
	"Mailgun webhook signing key":                   {},
	"Mapbox API token":                              {},
	"messagebird-api-token":                         {},
	"MessageBird API client ID":                     {},
	"New Relic user API Key":                        {},
	"New Relic user API ID":                         {},
	"New Relic ingest browser API token":            {},
	"npm access token":                              {},
	"Planetscale password":                          {},
	"Planetscale API token":                         {},
	"Postman API token":                             {},
	"Pulumi API token":                              {},
	"Rubygem API token":                             {},
	"Segment Public API token":                      {},
	"Sendgrid API token":                            {},
	"Sendinblue API token":                          {},
	"Sendinblue SMTP token":                         {},
	"Shippo API token":                              {},
	"Linkedin Client secret":                        {},
	"Linkedin Client ID":                            {},
	"Twitch API token":                              {},
	"Typeform API token":                            {},
	"Meta access token":                             {},
	"Oculus access token":                           {},
	"Instagram access token":                        {},
	"Yandex.Cloud IAM Cookie v1 - 1":                {},
	"Yandex.Cloud IAM Cookie v1 - 2":                {},
	"Yandex.Cloud IAM Cookie v1 - 3":                {},
	"Yandex.Cloud AWS API compatible Access Secret": {},
}

func TestGitleaksTOMLRegexp(t *testing.T) {
	gitleaks := parseGitleaksTOML(t)
	for _, rule := range gitleaks.Rules {
		if _, exists := ExcludedRules[rule.ID]; exists {
			continue
		}
		t.Run(rule.ID, func(t *testing.T) {
			r := regexp.MustCompile(rule.Regex)

			// generate a matching string
			token, err := reggen.Generate(rule.Regex, 100)
			require.NoError(t, err)

			// ensure matching in string doesn't
			concatMatch := fmt.Sprintf("SOMETEXT%sSOMETEXT", token)
			match := r.Match([]byte(concatMatch))
			require.Falsef(t, match, "pattern %q incorrectly matched %q ID: %s", rule.Regex, concatMatch, rule.ID)

			// ensure matching with password= works
			equalMatch := fmt.Sprintf("password=\"%s\"", token)
			match = r.Match([]byte(equalMatch))
			require.Truef(t, match, "pattern %q correctly matched %q ID: %s", rule.Regex, equalMatch, rule.ID)
		})
	}
}

func parseGitleaksTOML(t *testing.T) *GitleaksTOML {
	cfgReader, err := os.Open("./gitleaks.toml")
	require.NoError(t, err)

	gitleaks := &GitleaksTOML{}

	toml.NewDecoder(cfgReader).Strict(true).Decode(gitleaks)
	return gitleaks
}
